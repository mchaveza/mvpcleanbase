package mark.android.com.cleanbase.ui.presenters

import mark.android.com.cleanbase.base.BasePresenter
import mark.android.com.cleanbase.data.DataConfiguration
import mark.android.com.cleanbase.domain.LoginInteractor
import mark.android.com.cleanbase.ui.login.model.LoginRequest
import mark.android.com.cleanbase.ui.login.model.LoginResponse
import mark.android.com.cleanbase.ui.views.LoginView
import javax.inject.Inject

class LoginPresenter
@Inject
constructor(private val loginInteractor: LoginInteractor) : BasePresenter<LoginView>(), LoginInteractor.LoginCallback {

    init {
        setInteractorListener()
    }

    /**
     * With this we invoke the api call
     * to log into our account
     * @param user to be logged in
     * @param password that belongs to the user
     */
    fun login(user: String, password: String) {
        getView()?.showLoading()
        loginInteractor.login(buildLoginRequest(user, password))
    }

    /**
     * When login succeeded
     * @param loginResponse
     */
    override fun onSuccessLogin(loginResponse: LoginResponse) {
        getView()?.let {
            it.hideLoading()
            it.onLoginSucceeded(loginResponse)
        }
    }

    /**
     * When something went wrong with login
     * @param error
     */
    override fun onFailedLogin(error: Throwable) {
        getView()?.let {
            it.hideLoading()
            it.onLoginFailed(error)
        }
    }

    /**
     * Let's build LoginRequest object to invoke service
     * @param user
     * @param password
     */
    private fun buildLoginRequest(user: String, password: String): LoginRequest =
            LoginRequest(
                    DataConfiguration.CALL_ID,
                    DataConfiguration.CALL_ID_KEY,
                    user,
                    password
            )

    /**
     * Stop subscription once the activity has stopped
     * (if you want it that way)
     */
    fun stop(stop: Boolean = false) {
        if (stop) {
            loginInteractor.stop()
        }
    }

    private fun setInteractorListener() {
        this.loginInteractor.setListener(this)
    }

    @Suppress("DEPRECATION")
    fun setViewReference(view: LoginView) {
        setView(view)
        setInteractorListener()
    }

}



