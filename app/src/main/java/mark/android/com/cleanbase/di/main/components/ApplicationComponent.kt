package mark.android.com.cleanbase.di.main.components

import mark.android.com.cleanbase.ui.login.LoginFragment
import dagger.Component
import mark.android.com.cleanbase.base.BaseActivity
import mark.android.com.cleanbase.base.BaseActivityNoAuthentication
import mark.android.com.cleanbase.base.BaseFragment
import mark.android.com.cleanbase.di.data.DataModule
import mark.android.com.cleanbase.di.domain.DomainModule
import mark.android.com.cleanbase.di.main.modules.ApplicationModule
import mark.android.com.cleanbase.ui.home.HomeFragment
import mark.android.com.cleanbase.ui.splash.SplashActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class), (DataModule::class), (DomainModule::class)])
interface ApplicationComponent {
    fun inject(splashActivity: SplashActivity)
    fun inject(baseActivity: BaseActivity)
    fun inject(baseFragment: BaseFragment)
    fun inject(baseActivityNoAuthentication: BaseActivityNoAuthentication)
    fun inject(loginFragment: LoginFragment)
    fun inject(homeFragment: HomeFragment)
}