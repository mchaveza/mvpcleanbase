package mark.android.com.cleanbase.ui.login

import android.content.DialogInterface
import kotlinx.android.synthetic.main.fragment_login.*
import mark.android.com.cleanbase.CleanApplication
import mark.android.com.cleanbase.R
import mark.android.com.cleanbase.base.BaseFragment
import mark.android.com.cleanbase.ui.login.model.LoginResponse
import mark.android.com.cleanbase.ui.login.model.User
import mark.android.com.cleanbase.ui.presenters.LoginPresenter
import mark.android.com.cleanbase.ui.views.LoginView
import mark.android.com.cleanbase.utils.createDialog
import javax.inject.Inject

class LoginFragment : BaseFragment(), LoginView {

    @Inject
    lateinit var loginPresenter: LoginPresenter

    private lateinit var loginResponse: LoginResponse

    override fun getLayout(): Int =
            R.layout.fragment_login

    override fun initView() {
        super.initView()
        setupFragment()
        setListener()
    }

    private fun setListener() {
        this.login_btn.setOnClickListener {
            loginPresenter.login("mchaveza@ia.com.mx", "sky12.12")
        }
    }

    private fun setupFragment() {
        initializeDagger()
        loginPresenter.setViewReference(this)
    }

    override fun initializeDagger() {
        CleanApplication.getApplicationComponent().inject(this@LoginFragment)
    }

    override fun onLoginSucceeded(response: LoginResponse) {
        loginResponse = response
        (activity as LoginActivity).addLocalAccount(User("dummy@example.com",
                "dummyPassword",
                response.accessToken,
                response.expireIn.toString(),
                response.tokenType))
    }

    override fun onLoginFailed(throwable: Throwable) {
        createDialog(
                context = context!!,
                message = throwable.message.toString(),
                okBtn = getString(R.string.accept),
                positiveListener = DialogInterface.OnClickListener { dialog, _ ->
                    dialog.dismiss()
                }
        )
    }

    fun login() {
        (activity as LoginActivity).addLocalAccount(User("dummy@example.com",
                "dummyPassword",
                loginResponse.accessToken,
                loginResponse.expireIn.toString(),
                loginResponse.tokenType))
    }

    override fun showLoading() {
        loadingDialog.show(fragmentManager, LoginActivity::class.java.name)
    }

    override fun hideLoading() {
        loadingDialog.dismiss()
    }

    override fun onDetach() {
        super.onDetach()
        loginPresenter.stop()
    }

}