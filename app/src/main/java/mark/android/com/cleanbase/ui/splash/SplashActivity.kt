package mark.android.com.cleanbase.ui.splash

import android.Manifest
import android.accounts.AccountManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import com.ia.mchaveza.kotlin_library.PermissionManager
import mark.android.com.cleanbase.ui.home.HomeActivity
import mark.android.com.cleanbase.base.BaseAuthenticationActivity
import mark.android.com.cleanbase.ui.login.LoginActivity
import mark.android.com.cleanbase.ui.login.model.User

class SplashActivity : BaseAuthenticationActivity() {

    private val accountManager by lazy { AccountManager.get(this) }
    private val permissionManager by lazy { PermissionManager(this, null) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (permissionManager.permissionGranted(Manifest.permission.GET_ACCOUNTS)) {
            manageIntent(accountManager)
        }
    }

    private fun manageIntent(accountManager: AccountManager) {
        if (accountManager.getAccountsByType(this.packageName).isEmpty()) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onGetUser(user: User) {
    }

}
