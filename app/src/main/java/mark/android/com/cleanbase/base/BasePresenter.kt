package mark.android.com.cleanbase.base


open class BasePresenter<T : BaseView> {
    private var view: T? = null

    @Deprecated("Call setViewReference to avoid memory leaks.")
    fun setView(view: T?) {
        this.view = view
    }

    fun getView(): T? = this.view

}