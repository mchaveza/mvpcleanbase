package mark.android.com.cleanbase.data.net.entity

import mark.android.com.cleanbase.data.net.services.LoginService
import mark.android.com.cleanbase.data.net.utils.ResponseUtils
import mark.android.com.cleanbase.data.repository.LoginRepository
import mark.android.com.cleanbase.ui.login.model.LoginRequest
import mark.android.com.cleanbase.ui.login.model.LoginResponse
import rx.Observable

class NetLoginEntity(private val loginService: LoginService) : LoginRepository {

    /**
     * We call LOGIN service to authenticate user
     * and we handle success and failure responses
     */
    override fun login(loginRequest: LoginRequest): Observable<LoginResponse> =
            loginService.login(loginRequest.callId, loginRequest.callIdKey, loginRequest.user, loginRequest.password)
                    .flatMap {
                        if (it.isSuccessful) {
                            if (it.body().status == 0) {
                                Observable.just(it.body())
                            } else {
                                Observable.error(ResponseUtils.processWrongBody(it))
                            }
                        } else {
                            Observable.error(ResponseUtils.processErrorResponse(it))
                        }
                    }

}