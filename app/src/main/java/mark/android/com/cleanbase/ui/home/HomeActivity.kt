package mark.android.com.cleanbase.ui.home

import android.support.v4.app.Fragment
import mark.android.com.cleanbase.R
import mark.android.com.cleanbase.base.BaseActivity

class HomeActivity : BaseActivity() {

    private var homeFragment: HomeFragment? = null
    private var token: String? = null

    override fun getFragment(): Fragment {
        homeFragment = HomeFragment()
        return homeFragment!!
    }

    override fun initView() {
        super.initView()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.overridePendingTransition(R.anim.rigth_in, R.anim.rigth_out)
    }

}