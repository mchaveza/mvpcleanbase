package mark.android.com.cleanbase.di.main.modules

import android.app.Application
import android.content.Context
import com.ia.mchaveza.kotlin_library.GeocoderManager
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import dagger.Module
import dagger.Provides
import mark.android.com.cleanbase.CleanApplication
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: CleanApplication) {

    @Provides
    @Singleton
    fun providesApplication(): Application = application

    @Provides
    @Singleton
    fun providesContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun providesPreferencesHelper(context: Context): SharedPreferencesManager = SharedPreferencesManager(context)

    @Provides
    @Singleton
    fun providesTrackingManager(context: Context): TrackingManager = TrackingManager(context)

    @Provides
    @Singleton
    fun providesGeocoder(context: Context): GeocoderManager = GeocoderManager(context)

}