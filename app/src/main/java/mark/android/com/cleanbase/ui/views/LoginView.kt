package mark.android.com.cleanbase.ui.views

import mark.android.com.cleanbase.base.BaseView
import mark.android.com.cleanbase.ui.login.model.LoginResponse

interface LoginView : BaseView {
    fun onLoginSucceeded(response: LoginResponse)
    fun onLoginFailed(throwable: Throwable)
}