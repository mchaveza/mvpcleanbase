package mark.android.com.cleanbase.base

interface BaseAuthenticationView : BaseView {
    fun onTokenExpired(message: String)
}