package mark.android.com.cleanbase

import android.support.multidex.MultiDexApplication
import com.facebook.stetho.Stetho
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import mark.android.com.cleanbase.di.data.DataModule
import mark.android.com.cleanbase.di.main.components.ApplicationComponent
import mark.android.com.cleanbase.di.main.components.DaggerApplicationComponent
import mark.android.com.cleanbase.di.main.modules.ApplicationModule

class CleanApplication : MultiDexApplication() {

    private lateinit var preferences: SharedPreferencesManager

    companion object {
        private lateinit var instance: CleanApplication

        private lateinit var applicationComponent: ApplicationComponent

        private fun initDependencies(application: CleanApplication) {
            applicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(ApplicationModule(application))
                    .dataModule(DataModule(application.getString(R.string.base_url)))
                    .build()
        }

        fun getApplicationComponent() = applicationComponent

        fun getInstance() = instance
    }

    override fun onCreate() {
        super.onCreate()
        initDependencies(this)
        Stetho.initializeWithDefaults(this)
        instance = this@CleanApplication
    }

}