package mark.android.com.cleanbase.data.net.utils

import android.text.TextUtils
import com.google.gson.Gson
import mark.android.com.cleanbase.base.BaseResponse
import mark.android.com.cleanbase.data.ArgumentConstants
import mark.android.com.cleanbase.exception.*
import retrofit2.Response
import java.io.IOException

class ResponseUtils {

    private constructor() {}

    companion object {
        /**
         * Obtains a failure response and transform it into an AppException
         * @param response Error response from retrofit
         * @return Custom Orsan Exception
         */
        fun processErrorResponse(response: Response<*>): AppException {
            try {
                val errorBody = response.errorBody().string()
                return if (TextUtils.isEmpty(errorBody)) {
                    when (response.code()) {
                        504 -> AppNetworkException(ArgumentConstants.NO_INTERNET)
                        else -> AppUnknownException(response.message(), response.code())
                    }
                } else {
                    val gson = Gson()
                    val error = gson.fromJson(errorBody, Error::class.java)
                    val codeError = response.code()
                    AppHttpException(error.message.toString(), codeError)
                }
            } catch (ioException: IOException) {
                return AppUnknownException("Error parsing message", ioException, response.code())
            }
        }

        /**
         * Obtains a failure response out of a
         * 200 OK when it exists
         * @param response
         */
        fun processWrongBody(response: Response<*>): AppException {
            return try {
                val body = response.body()
                if ((body as BaseResponse).message == "Token no valido!!\"" || body.message == "Token no valido!!") {
                    AppExpiredException(body.message)
                } else {
                    AppException(body.message)
                }
            } catch (ioException: IOException) {
                AppUnknownException("Error parsing message", ioException, response.code())
            }
        }
    }

}