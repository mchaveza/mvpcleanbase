package mark.android.com.cleanbase.domain

import mark.android.com.cleanbase.data.ArgumentConstants
import mark.android.com.cleanbase.data.repository.LoginRepository
import mark.android.com.cleanbase.exception.AppException
import mark.android.com.cleanbase.exception.AppNetworkException
import mark.android.com.cleanbase.ui.login.model.LoginRequest
import mark.android.com.cleanbase.ui.login.model.LoginResponse
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.IOException

class LoginInteractor(private val repository: LoginRepository) {

    private var mListener: LoginCallback? = null
    private var subscription: Subscription? = null

    fun setListener(listener: LoginCallback) {
        mListener = listener
    }

    fun login(loginRequest: LoginRequest) {
        subscription = repository.login(loginRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ loginResponse ->
                    mListener?.onSuccessLogin(loginResponse)
                }, { error ->
                    mListener?.let {
                        when (error) {
                            is IOException -> it.onFailedLogin(AppNetworkException(ArgumentConstants.NO_INTERNET, error))
                            is AppException -> it.onFailedLogin(error)
                            else -> it.onFailedLogin(AppException("Unknown exception", error))
                        }
                    }
                })
    }

    fun stop() {
        subscription?.unsubscribe()
    }

    interface LoginCallback {
        fun onSuccessLogin(loginResponse: LoginResponse)
        fun onFailedLogin(error: Throwable)
    }

}