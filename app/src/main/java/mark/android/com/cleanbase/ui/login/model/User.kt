package mark.android.com.cleanbase.ui.login.model

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

data class User(var email: String,
                var password: String,
                var accessToken: String,
                var expireIn: String,
                var tokenType: String
) : Serializable, Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()
    ) {
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(email)
        dest.writeString(password)
        dest.writeString(accessToken)
        dest.writeString(expireIn)
        dest.writeString(tokenType)
    }

    override fun describeContents(): Int =
            0

    companion object CREATOR : Parcelable.Creator<User> {
        override fun createFromParcel(parcel: Parcel): User {
            return User(parcel)
        }

        override fun newArray(size: Int): Array<User?> {
            return arrayOfNulls(size)
        }
    }

    val bundle: Bundle
        get() {
            val b = Bundle()
            b.putString("email", email)
            b.putString("password", password)
            b.putString("accessToken", accessToken)
            b.putString("expireIn", expireIn)
            b.putString("tokenType", tokenType)
            return b
        }
}