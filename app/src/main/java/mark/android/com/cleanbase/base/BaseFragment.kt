package mark.android.com.cleanbase.base

import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ia.mchaveza.kotlin_library.GeocoderManager
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import com.ia.mchaveza.kotlin_library.TrackingManagerLocationCallback
import mark.android.com.cleanbase.CleanApplication
import mark.android.com.cleanbase.ui.dialogs.LoadingDialog
import javax.inject.Inject

abstract class BaseFragment : Fragment(), TrackingManagerLocationCallback {

    @Inject
    lateinit var preferences: SharedPreferencesManager

    @Inject
    lateinit var trackingManager: TrackingManager

    @Inject
    lateinit var geocoderManager: GeocoderManager

    lateinit var loadingDialog: LoadingDialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(getLayout(), container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    open fun initView() {
        initializeDagger()
        loadingDialog = LoadingDialog()
        loadingDialog.isCancelable = false
    }

    open fun initializeDagger() {
        CleanApplication.getApplicationComponent().inject(this)
    }

    override fun onLocationHasChanged(location: Location) {
    }

    override fun onLocationHasChangedError(error: Exception) {
    }

    abstract fun getLayout(): Int

}