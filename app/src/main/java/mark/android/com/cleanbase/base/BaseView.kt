package mark.android.com.cleanbase.base

interface BaseView{
    fun showLoading()
    fun hideLoading()
}