package mark.android.com.cleanbase.data.net.services

import mark.android.com.cleanbase.data.DataConfiguration
import mark.android.com.cleanbase.ui.login.model.LoginResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST
import rx.Observable

interface LoginService {

    @Headers("Content-Type: application/x-www-form-urlencoded; charset=UTF-8")
    @FormUrlEncoded
    @POST(DataConfiguration.LOGIN)
    fun login(
            @Field("CallId") callId: String,
            @Field("CallIdKey") callIdKey: String,
            @Field("usuario") user: String,
            @Field("pass") pass: String
    ): Observable<Response<LoginResponse>>

}