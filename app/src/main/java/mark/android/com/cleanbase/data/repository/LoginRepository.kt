package mark.android.com.cleanbase.data.repository

import mark.android.com.cleanbase.ui.login.model.LoginRequest
import mark.android.com.cleanbase.ui.login.model.LoginResponse
import rx.Observable

interface LoginRepository {

    fun login(loginRequest: LoginRequest): Observable<LoginResponse>

}