package mark.android.com.cleanbase.exception

class AppNetworkException : AppException {

    constructor(detailedMessage: String) : super(detailedMessage) {}

    constructor(detailedMessage: String, throwable: Throwable) : super(detailedMessage, throwable) {}

}