package mark.android.com.cleanbase.ui.dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.widget.TextView
import mark.android.com.cleanbase.R

class LoadingDialog : DialogFragment() {

    private var message: TextView? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogView = LayoutInflater.from(activity).inflate(R.layout.dialog_loading, null)
        message = dialogView.findViewById(R.id.dialog_loading_message)
        return AlertDialog.Builder(activity).setView(dialogView).create()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

}