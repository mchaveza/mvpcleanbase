package mark.android.com.cleanbase.ui.home

import mark.android.com.cleanbase.CleanApplication
import mark.android.com.cleanbase.R
import mark.android.com.cleanbase.base.BaseFragment
import mark.android.com.cleanbase.data.ArgumentConstants
import mark.android.com.cleanbase.ui.login.model.User

class HomeFragment : BaseFragment() {

    private var user: User? = null

    override fun getLayout(): Int =
            R.layout.fragment_home

    override fun initView() {
        super.initView()
        initializeDagger()
        recoverUser()
    }

    private fun recoverUser() {
        val bundle = arguments
        this.user = bundle?.getSerializable(ArgumentConstants.USER) as? User
    }

    override fun initializeDagger() {
        CleanApplication.getApplicationComponent().inject(this)
    }

}