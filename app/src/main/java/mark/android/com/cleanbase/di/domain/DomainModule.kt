package mark.android.com.cleanbase.di.domain

import dagger.Module
import dagger.Provides
import mark.android.com.cleanbase.data.repository.LoginRepository
import mark.android.com.cleanbase.domain.LoginInteractor
import javax.inject.Singleton

@Module
class DomainModule {

    @Provides
    fun provideLoginInteractor(repository: LoginRepository) =
            LoginInteractor(repository)

}