package mark.android.com.cleanbase.ui.login

import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.os.Bundle
import android.support.v4.app.Fragment
import mark.android.com.cleanbase.account.Authenticator
import mark.android.com.cleanbase.base.BaseActivityNoAuthentication

class LoginActivity : BaseActivityNoAuthentication() {

    private var mResultBundle: Bundle? = null
    private var loginFragment: LoginFragment? = null
    private var mAccountAuthenticatorResponse: AccountAuthenticatorResponse? = null

    override fun getFragment(): Fragment {
        loginFragment = LoginFragment()
        return loginFragment!!
    }

    override fun initView() {
        super.initView()
        setupAccountManager()
    }


    override fun finish() {
        if (this.mAccountAuthenticatorResponse != null) {
            // send the result bundle back if set, otherwise send an error.
            if (this.mResultBundle != null) {
                this.mAccountAuthenticatorResponse?.onResult(this.mResultBundle)
            } else {
                this.mAccountAuthenticatorResponse?.onError(AccountManager.ERROR_CODE_CANCELED,
                        "canceled")
            }
            this.mAccountAuthenticatorResponse = null
        }
        super.finish()
    }

    private fun setupAccountManager() {
        this.mAccountAuthenticatorResponse = intent.getParcelableExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE)
        if (this.mAccountAuthenticatorResponse != null) {
            this.mAccountAuthenticatorResponse?.onRequestContinued()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Authenticator.REQUEST_PERMISSIONS_CODE -> {
                loginFragment?.login()
            }
        }
    }

}